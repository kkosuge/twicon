root = '/usr/local/sinatra/cache.twisave.com/current'

bind "unix://#{root}/tmp/sockets/puma.sock"
pidfile "#{root}/tmp/pid"
pidfile "#{root}/tmp/pids/puma.pid"
state_path "#{root}/tmp/pids/puma.state"
activate_control_app
