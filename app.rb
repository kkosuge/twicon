require 'sinatra'
require_relative 'lib/icon/icon'
configure { set :server, :puma }

class App < Sinatra::Base
  get '/icon/*/*' do |screen_name, size|
    icon = Icon.new(screen_name, size)
    send_file icon.path, type: icon.mime_type, disposition: 'inline'
  end

  get '/icon/*' do |screen_name|
    icon = Icon.new(screen_name, 'middle')
    send_file icon.path, type: icon.mime_type, disposition: 'inline'
  end
end
