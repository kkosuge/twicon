require 'open-uri'
Bundler.require

class Icon
  %w( original large middle small ).each do |size|
    FileUtils.mkdir_p Bundler.root + "public/icon/#{size}"
  end

  def initialize(screen_name, size="large")
    @screen_name = screen_name
    @size = size
    save unless exist?
  end

  def exist?
    File.exist?(middle_file_path)
  end

  def path
    base_path + "#{@size}/#{@screen_name}"
  end

  def image_url
    doc = Nokogiri::HTML(open("https://twitter.com/#{@screen_name}", open_uri_options))
    doc.css('.profile-picture').first[:href]
  end

  def save
    @image = MiniMagick::Image.open(image_url)
    # @image.write(original_file_path)
    narrow = @image[:width] > @image[:height] ? @image[:height] : @image[:width]
    @image.combine_options do |c|
     c.gravity "center"
     c.crop "#{narrow}x#{narrow}+0+0"
    end
    #@image.write(large_file_path)
    @image.resize("128x128")
    @image.write(middle_file_path)
    @image.resize("64x64")
    @image.write(small_file_path)
    true
  end

  def mime_type
    MimeMagic.by_magic(File.open(middle_file_path)).type
  end

  def root_path
    Bundler.root
  end

  def base_path
    root_path + "public/icon/"
  end

  def original_file_path
    base_path + "original/#{@screen_name}"
  end

  def large_file_path
    base_path + "large/#{@screen_name}"
  end

  def middle_file_path
    base_path + "middle/#{@screen_name}"
  end

  def small_file_path
    base_path + "small/#{@screen_name}"
  end

  private

  def open_uri_options
    {
      "User-Agent" => "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)"
    }
  end
end
